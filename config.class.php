<?php
define('DEBUG',1);					//是否开启debug调试模式 建议在生产环境中关闭
define('URL_STYLE','/');				//默认分割url样式，形如controls-action-value-value格式
define('TPL_CACHE_SET',0);				//是否开启模板缓存
define('TPL_CACHE_TIME',60*60*24);			//模板缓存时间
define('TPL_SUFFIX','html');				//模板后缀

define('TABLE_PREFIX','');				//数据表表前缀
$db_config = array(
	'database_type' => 'mysql',
	'database_name' => 'mcms',
	'server' => 'localhost',
	'username' => 'root',
	'password' => 'dream',
	'charset' => 'utf8',
	// driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
	'option' =>array(
		PDO::ATTR_CASE => PDO::CASE_NATURAL
	),
);