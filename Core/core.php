<?php
define('CORE_CODE','1.0.0');						//core核心版本
header("Content-Type:text/html;charset=utf-8");		//设置系统的输出字符为utf-8
date_default_timezone_set('PRC');					//设置时区为中国
if (!defined('IN_CORE')){
	die('Hacking attempt!');
}
//define('DS',DIRECTORY_SEPARATOR);
define('DS','/');
define('WEBROOT_PATH',getcwd().DS);						//基准目录
define('CORE_PATH',rtrim(WEBROOT_PATH.CORE_NAME,DS));				//框架目录
define('APP_PATH',rtrim(WEBROOT_PATH.APP_NAME,DS));		//项目目录
//设置debug
if(defined('DEBUG') && DEBUG==true){
	$GLOBALS["debug"]=1;                 //初例化开启debug
	error_reporting(E_ALL ^ E_NOTICE);   //输出除了注意的所有错误报告
	include 'debug.class.php'; 			 //包含debug类
	Debug::start();                               //开启脚本计算时间
	set_error_handler(array("Debug", 'Catcher')); //设置捕获系统异常
}else{
	ini_set('display_errors', 'Off'); 		//屏蔽错误输出
	ini_set('log_errors', 'On');             	//开启错误日志，将错误报告写入到日志中
	ini_set('error_log', WEBROOT_PATH.'runtime'.DS.APP_NAME.DS.'error_log'); //指定错误日志文件
}
// 关闭Magic_Quotes
if(get_magic_quotes_gpc()){
	function stripslashes_deep($value){
		$value = is_array($value) ? array_map('stripslashes_deep', $value) : (isset($value) ? stripslashes($value) : null);
		return $value;
	}
	$_POST = stripslashes_deep($_POST);
	$_GET = stripslashes_deep($_GET);
	$_COOKIE = stripslashes_deep($_COOKIE);
}
//默认框架必须目录，若不存在则自动创建
if(!file_exists(APP_PATH.DS.'contrils'.DS)){
	$core_dirs=array(
		APP_PATH.DS,
		APP_PATH.DS.'controls'.DS,		//应用控制文件夹
		APP_PATH.DS.'models'.DS,		//应用模型文件夹
		APP_PATH.DS.'views'.DS,			//应用模板文件夹
	);
	core_mkdirs($core_dirs);
	unset($core_dirs);
}
if(!file_exists(APP_PATH.DS.'runtime'.DS)){
	$core_dirs=array(
		WEBROOT_PATH.'runtime'.DS.APP_NAME.DS,		//缓存主目录
		WEBROOT_PATH.'runtime'.DS.APP_NAME.DS.'comps',	//模板编译
		WEBROOT_PATH.'runtime'.DS.APP_NAME.DS.'cache',	//模板缓存
		WEBROOT_PATH.'runtime'.DS.APP_NAME.DS.'error_log',	//错误日志
	);
	core_mkdirs($core_dirs);
	unset($core_dirs);
}
//include默认包含路径
$core_include_path=get_include_path();					//原基础目录
$core_include_path.=PATH_SEPARATOR.CORE_PATH.DS;		//基础类
$core_include_path.=PATH_SEPARATOR.CORE_PATH.DS.'smarty'.DS ;	//模板 smarty类库
$core_include_path.=PATH_SEPARATOR.APP_PATH.DS.'controls'.DS;	//应用程序 控制器类
$core_include_path.=PATH_SEPARATOR.APP_PATH.DS.'libs'.DS;		//应用程序 自建libs类库
set_include_path($core_include_path);							//设置include包含文件所在的所有目录	
	// 获取请求的地址
$core_uri = null;
if(isset($_SERVER['PATH_INFO'])) $core_uri = $_SERVER['PATH_INFO'];
if(isset($_SERVER['ORIG_PATH_INFO'])) $core_uri = $_SERVER['ORIG_PATH_INFO'];
if(isset($_SERVER['SCRIPT_URL'])) $core_uri = $_SERVER['SCRIPT_URL'];
$core_uri = trim($core_uri,'/');
$core_uri_array = explode(URL_STYLE,$core_uri);
$core_uri_count=count($core_uri_array);
$core_class = $core_uri_array[0] ? $core_uri_array[0] : 'index';
$core_method = isset($core_uri_array[1]) ? $core_uri_array[1] : 'index';
$core_pramers=array();
if($core_uri_count>2){
	if($core_uri_count%2==0){
		for($i=2;$i<$core_uri_count;$i+=2){
			$_GET[$core_uri_array[$i]]=$core_uri_array[$i+1];
		}

	}else{
		core_show404('URL参数不匹配！');
	}
}
$core_class_path=APP_PATH.DS.'controls'.DS.$core_class.'.class.php';
//判断控制器类是否存在，不存在则调用404页面
if(!file_exists($core_class_path))core_show404("类文件不存在!($core_class_path)");
if(!class_exists($core_class))core_show404('类名不存在!');
if(!method_exists($core_class,$core_method))core_show404('方法名不存在!');
session_start();
Debug::addmsg('开启Session会话:'.session_id());
// 载入数据库
if(isset($db_config)){
	$db = new m($db_config);
}
//加载公共文件init.php
$core_init_path = APP_PATH.DS.'controls'.DS.'init.php';
if(is_file($core_init_path)){
	include($core_init_path); 
}
//模板文件中所有要的路径，html\css\javascript\image\link等中用到的路径，从WEB服务器的文档根开始
$spath=rtrim(substr(dirname(str_replace('\\', '/', dirname(__FILE__))), strlen(rtrim($_SERVER["DOCUMENT_ROOT"],DS))), '/\\');
$GLOBALS['sroot']=$spath.DS;
$GLOBALS['root']='http://'.$_SERVER['HTTP_HOST'].$spath.DS; //Web服务器根到项目的根
$GLOBALS['public']=$GLOBALS['root'].'public'.DS;        //全局资源目录
if(APP_NAME==''){
	$GLOBALS['res']=rtrim(dirname($_SERVER["SCRIPT_NAME"]),'/\\').DS.'views'.DS.'res'.DS; 
}else{
	$GLOBALS['res']=rtrim(dirname($_SERVER["SCRIPT_NAME"]),'/\\').DS.APP_NAME.DS.'views'.DS.'res'.DS; 
}
//当前应用模板的资源
$GLOBALS['app']=$_SERVER['SCRIPT_NAME'].DS;           	//当前应用脚本文件
$GLOBALS['url']=$GLOBALS['app'].$core_class.DS;       //访问到当前模块
$GLOBALS['m']=$core_class;           	
$GLOBALS['a']=$core_method;
//加载控制类
$core = new $core_class();
$core->$core_method();
Debug::addmsg("正在访问 控制器:<b>$core_class</b> 方法:<b>$core_method</b>");
Debug::addmsg("当前控制器类:<b>$core_class_path</b> 文件!");
//设置输出Debug模式的信息
if(defined('DEBUG') && DEBUG==1 && $GLOBALS["debug"]==1){
	Debug::stop();
	Debug::message();
}
//##############核心函数################
/*
 * 自动加载类库
 */
function __autoload($core_class) {
	$core_class=strtolower($core_class);
	include($core_class.'.class.php');
	Debug::addmsg("<b>载入 $core_class </b>类", 1);  //在debug中显示自动包含的类
}
/*
 * 显示404错误信息
 */
function core_show404($msg='访问页面不存在!'){
	//设置输出Debug模式的信息
	if(defined('DEBUG') && DEBUG==1 && $GLOBALS["debug"]==1){
		Debug::addmsg($msg);
		Debug::stop();
		Debug::message();
	}else{
		header("HTTP/1.1 404 Not Found");
	}
	exit(1);
}
/*
 * 创建目录
 * @param	string	$dirs		需要创建的目录名称
 */
function core_mkdirs($dirs){
	foreach($dirs as $dir){
		if(!file_exists($dir)){
			if(mkdir($dir,"0755")){
				Debug::addmsg('创建目录'.$dir.'成功');
			}
		}
	}
}
/**
 * 关闭调试模式
 * @param	bool	$falg	调式模式的关闭开关
 */
function core_debug($falg=0){
	$GLOBALS["debug"]=$falg;
}