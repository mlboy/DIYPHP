<?php
class m extends medoo{
	public function select($table, $columns,$where = null)
	{
		$result = parent::select($table.TABLE_PREFIX, $columns,$where);
		Debug::addmsg($this->last_query(), 2);
		$error=$this->error();
		if($error[0]!='00000')
			Debug::addmsg('第<b>'.$error[1].'</b>号错误：'.$error[2], 2);
		return $result;
		
	}	
	public function insert($table, $data)
	{
		$result = parent::insert($table.TABLE_PREFIX, $data);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}
	
	public function update($table, $data, $where = null)
	{
		$result = parent::update($table.TABLE_PREFIX, $data, $where);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}
	
	public function delete($table, $where)
	{
		$result = parent::delete($table.TABLE_PREFIX, $where);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}
	
	public function replace($table, $columns, $search = null, $replace = null, $where = null)
	{
		$result = parent::replace($table.TABLE_PREFIX, $columns, $search, $replace, $where);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}

	public function get($table, $columns, $where = null)
	{
		$result = parent::get($table.TABLE_PREFIX, $columns, $where);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}

	public function has($table, $where)
	{
		$result = parent::has($table.TABLE_PREFIX, $where);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}

	public function count($table, $where = null)
	{
		$result = parent::count($table.TABLE_PREFIX, $where);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}

	public function max($table, $column, $where = null)
	{
		$result = parent::max($table.TABLE_PREFIX, $column, $where);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}

	public function min($table, $column, $where = null)
	{
		$result = parent::min($table.TABLE_PREFIX, $column, $where);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}

	public function avg($table, $column, $where = null)
	{
		$result = parent::avg($table.TABLE_PREFIX, $column, $where);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}

	public function sum($table, $column, $where = null)
	{
		$result = parent::sum($table.TABLE_PREFIX, $column, $where);
		Debug::addmsg($this->last_query(), 2);
		return $result;
	}
}