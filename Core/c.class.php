<?php
if (!defined('IN_CORE')){
	die('Hacking attempt!');
}
class c extends v{ 
	function __construct()
	{
		parent::__construct();         //调用父类被覆盖的构造方法
	}
	function index()
	{
		echo "页面正在构建中……";
	}
	function redirect($path, $args=""){
		$path=trim($path, '/');
		if($args!="")
			$args="/".trim($args, '/');
		if(strstr($path, '/')){
			$url=$GLOBALS['app'].$path.$args;
		}else{
			$url=$GLOBALS['url'].$path.$args;
		}
		//使用js跳转前面可以有输出
		echo '<script>';
		echo 'location="'.$url.'"';
		echo '</script>';
	}
	/**
	 * 消息提示框
	 * @param	string	$mess		用示输出提示消息
	 * @param	bool	$mark	成功图标还是失败图标
	 * @param	int	$timeout	设置跳转的时间，单位：秒
	 * @param	string	$location	设置跳转的新位置
	 */
	function message($mess='操作成功',$mark=true, $timeout=3, $location=''){	
		core_debug(0);
		$this->caching=0;     //设置缓存关闭
		if($location==''){
			$location="window.history.back();";
		}else{
			$path=trim($location, '/');
			if(strstr($path, '/')){
				$location=$GLOBALS['app'].$path;
			}else{
				$location=$GLOBALS['url'].$path;
			}
			$location="window.location='{$location}'";
		}
		$this->assign('mess', $mess);
		$this->assign('mark', $mark);
		$this->assign('timeout', $timeout);
		$this->assign('location', $location);
		$this->display('public/message');
		exit;
	}
}